// JS file for cycleTracker app

// -------------
// Variable declarations
// -------------
const newBdayFormEl = document.getElementsByTagName("form")[0];
const dayInputEl = document.getElementById("day");
const monthInputEl = document.getElementById("month");
const yearInputEl = document.getElementById("year");
const nameInputEl = document.getElementById("name");
const birthdaysContainer = document.getElementById("all-bdays");

// Storage key is an app-wide constant
const STORAGE_KEY = "birthday-tracker";

// -------------
// Event Handlers
// -------------
newBdayFormEl.addEventListener("submit", (event) => {
  event.preventDefault();
  const day = dayInputEl.value.padStart(2, '0');
  const month = monthInputEl.value;
  const year = yearInputEl.value;
  const name = nameInputEl.value;
  if (isDateAfterToday(day, month, year)) {
        return;
  }
  storeNewBirthday(day, month, year, name);
  renderBirthdays();
  newBdayFormEl.reset();
});

// -------------
// Functionality
// -------------

// 1. Form validation
function isDateAfterToday(day, month, year) {
    today = new Date(year, month, day);
    if (new Date(today.toDateString()) > new Date(new Date().toDateString())) {
        newBdayFormEl.reset();
        return true;

    }
    return false;
}

// 2. Get, add, sort, and store data
function storeNewBirthday(day, month, year, name) {
    const birthdays = getAllStoredBirthdays();
    birthdays.push({ day, month, year, name });
    birthdays.sort((a, b) => {
        return (parseInt(a.month + a.day) - parseInt (b.month + b.day));
  });

    window.localStorage.setItem(STORAGE_KEY, JSON.stringify(birthdays));
}

// 3. Get and parse data
function getAllStoredBirthdays() {
  const data = window.localStorage.getItem(STORAGE_KEY);
  const birthdays = data ? JSON.parse(data) : [];
  return birthdays;
}

// 4. Display data
function renderBirthdays() {
  const birthdaysHeader = document.createElement("h2");
  const birthdaysList = document.createElement("ul");
  const birthdays = getAllStoredBirthdays();
  if (birthdays.length === 0) {
    return;
  }
  birthdaysContainer.innerHTML = "";
  birthdaysHeader.textContent = "Birthdays";
  const birthdayTable = document.createElement("table");
  birthdays.forEach((birthday) => {
    const tRow = document.createElement("tr");
    const tdInfo = document.createElement("td");
    const tdButton = document.createElement("td");
    const bday_year = `${birthday.year}` ? `[${birthday.year}]` : '';
    tdInfo.textContent = `${birthday.day}/${birthday.month} - ${birthday.name} ${bday_year} `;

    var button = document.createElement("input");
    button.type = "button";
    button.value = "Delete";
    //button.onclick = func;
      tdButton.appendChild(button);
      tRow.appendChild(tdInfo);
      tRow.appendChild(tdButton);
      birthdayTable.appendChild(tRow);
  });

    birthdaysContainer.appendChild(birthdaysHeader);
    birthdaysContainer.appendChild(birthdayTable);
}

// -------------
// Call render on page load
// -------------

renderBirthdays();
