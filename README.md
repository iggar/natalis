Natalis

Keep track of friends and family birthdays

ALl data is kept locally to your browser. No data is sent out to the Internet. 
If you want to sync data with another device, for now you will need to repeat adding the items. A export/import functionality is on the roadmap (see tasks list below).


### TODO 
- [ ] Tidy up CSS (remove unused li styles, fix colours for bday table)
- [ ] Delete birthday
- [ ] Edit birthday
- [ ] Better style for colours and "Add" button
- [ ] Calculate, when possible, current age
- [ ] Export / import list using stringify + parse
- [ ] Indicate next ones, highlight today's bday
- [ ] Notification on bdays approaching / happening today
